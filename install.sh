#!/usr/bin/env bash
#echo "installing yarn ..."
#npm install -g yarn --registry=https://registry.npm.taobao.org
#echo "installing truffle ..."
#npm install -g truffle --registry=https://registry.npm.taobao.org
#echo "yarn installing projects packages ..."

yarn install

truffleDir=$(which truffle)
truffleDir=`realpath ${truffleDir}`
truffleDir=`dirname ${truffleDir}`
truffleDir=`dirname ${truffleDir}`
cd ${truffleDir}
echo "update solidity to @0.4.18"
yarn add solc@0.4.18

echo "installed complete!"
