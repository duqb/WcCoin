const WcCoin = artifacts.require("./WcCoin.sol");
const Web3 = require("web3");
const web3 = new Web3();


contract('WcCoin', function(accounts) {

  it("should send coin correctly", function() {
    let wccoin;

    // Get initial balances of first and second account.
    const account_one = accounts[0];
    const account_two = accounts[1];

    let account_one_starting_balance;
    let account_two_starting_balance;
    let account_one_ending_balance;
    let account_two_ending_balance;

    const amount = web3.toWei(10);

    return WcCoin.deployed().then(function(instance) {
      wccoin = instance;
      console.log(`WcCoin contract address:`, wccoin.address);
      return wccoin.balanceOf(account_one);
    }).then(function(balance) {
      account_one_starting_balance = balance.toNumber();
      return wccoin.balanceOf(account_two);
    }).then(function(balance) {
      account_two_starting_balance = balance.toNumber();
      return wccoin.transfer(account_two, amount, {from: account_one});
    }).then(function() {
      return wccoin.balanceOf(account_one);
    }).then(function(balance) {
      account_one_ending_balance = balance.toNumber();
      return wccoin.balanceOf(account_two);
    }).then(function(balance) {
      account_two_ending_balance = balance.toNumber();

      assert.equal(account_one_ending_balance, account_one_starting_balance - amount, "Amount wasn't correctly taken from the sender");
      assert.equal(account_two_ending_balance, account_two_starting_balance + amount, "Amount wasn't correctly sent to the receiver");
      console.log(`transfer 10.0WC from ${account_one} → ${account_two}`);
    }).then(()=>{
      return wccoin.balanceOf(account_one);
    }).then(d=>{
      console.log(`account one:${account_one} has ${web3.fromWei(d.toString())}WC`);
    });


  });
});
