module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // to customize your Truffle configuration!

  networks : {
    development: {
      from: "0x02381B2c97354453BabDf11Bee088a81f78A15b3",
      host: "10.0.11.50",
      port: 7545,
      network_id: "4777", // Match any network id
    }
  }
};
